FROM openjdk:17-ea-10-jdk
ADD build/libs/docker-first-api-0.0.1-SNAPSHOT.jar .
EXPOSE 9010
CMD java -jar docker-first-api-0.0.1-SNAPSHOT.jar