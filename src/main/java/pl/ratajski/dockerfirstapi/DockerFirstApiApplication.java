package pl.ratajski.dockerfirstapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerFirstApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DockerFirstApiApplication.class, args);
    }

}
