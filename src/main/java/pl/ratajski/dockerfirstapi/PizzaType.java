package pl.ratajski.dockerfirstapi;

public class PizzaType {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PizzaType(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
