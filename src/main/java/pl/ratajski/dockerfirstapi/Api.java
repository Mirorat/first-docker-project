package pl.ratajski.dockerfirstapi;

import com.google.gson.Gson;

import com.google.gson.JsonArray;
import com.google.gson.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.XML;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
public class Api {

    private List<PizzaType> pizzaTypes;

    public Api() {
        this.pizzaTypes = new ArrayList<>();
        pizzaTypes.add(new PizzaType(1, "Capriciosa"));
        pizzaTypes.add(new PizzaType(2, "Funghi"));
        pizzaTypes.add(new PizzaType(3, "Calzone"));
        pizzaTypes.add(new PizzaType(4, "Margherita"));
    }

    @GetMapping("/pizzatypes")
    public List<PizzaType> getPizzaTypes() {
        return pizzaTypes;
    }

    @GetMapping(value="/pizzatypesxml", produces= MediaType.APPLICATION_ATOM_XML_VALUE)
    public String getPizzasXML() {

        JSONArray jsonPizzaTypes = new JSONArray(pizzaTypes);
        Optional<String> pizzaTypesXML= Optional.empty();
        try {
            pizzaTypesXML = Optional.of(XML.toString(jsonPizzaTypes));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return pizzaTypesXML.orElse("No Values");
    }


}

